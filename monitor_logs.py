#!/usr/bin/python3
import os, re
import bin

directory = '/sites/PROD/var/log/'
data = os.listdir(directory)
files = [f for f in data  if f.split('.')[-1] == 'log']

print('-------------------------------------------------------------------------------\n')
try:
    for file_log in files:
        log_err2 = {'flag': False,
                    'txt': '',
                    'file_log':'',
                    }
        with open(directory + file_log, 'r', encoding='utf-8') as f:
            print('читаем файл: ' + file_log)
            for line in f:
                if bin.test_date(line[0:17]):
                    if re.search(r'процесс|error|fatal|ошибка',line, re.I|re.U) :
                        log_err2['flag'] = True
                        log_err2['txt'] = line
                        log_err2['file_log'] = file_log

        if log_err2['flag']:
            err_txt = f"ERROR-> {log_err2['file_log']} : {log_err2['txt']}"
            print(err_txt)
            #bin.send_msg_sms(err_txt)
            #bin.send_msg_post(err_txt)
            bin.send_telegram(err_txt)

except Exception as ex:
    print(ex)

print('-------------------------------------------------------------------------------\n')
#files = bin.clear_log(files)