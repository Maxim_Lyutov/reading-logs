for ((;;))
    do
    clear
    
    cd /mnt/sites/PROD/var/log/
    grep -Eci 'процесс|error|fatal|ошибка' *.log | sed s'/:/;/' > ~/res.csv
    sed -i -e '1 s/^/name_script;error_count\n/' ~/res.csv ;

    cat ~/res.csv
    echo '-----------';
    grep -Ei 'процесс|error|fatal|ошибка' *.log

    current_date=$(date +"%H")
    echo
    if [ "$current_date" -lt 10 ]
    then
    echo 'доп логи :'
    grep -Ei 'процесс|error|fatal|ошибка' *.log.1 | grep -Ei ' 0'
    fi

    echo
    date

    cd

   #./bin/monitor_log.py
    sleep 300
