import requests, os, datetime, smtplib

def clear_log(files =('get_odds_from_mysql.log',)):
    for f in files:
        try:
            os.remove(f)
        except Exception as ex:
            print(ex)
        else:
            return (files)

def send_msg_sms2(msg='сообщение: ОШИБКА в обработке данных...'):
    print('!!!ЗАГЛУШКА : ')

def send_msg_sms(msg='сообщение: ОШИБКА в обработке данных...'):
    url = f"https://sms.ru/sms/send"
    data = {'api_id' : '49FXXXXB-FXXA-EXX7-2XXB-6FXXXXXXX5',
            'to' : '790000000',
            'msg' : msg,
            'json' : 1,
    }
    response = requests.post(url, data=data)
    if response == 200 :
        print(f'сообщение доставлено : {response}')
    else:
        print(response)

def send_msg_post(txt_err=''):
    srv = smtplib.SMTP('mail-sender.ak.local')
    fromaddr = 'm.xxxxov@axxxxxent.ru'
    toaddr = ['axxxx@mxxxx.ru',]

    subj = 'ВНИМАНИЕ : Не работает обмен '
    msg_txt = 'Найдены проблемы на сервере : ' +  txt_err
    msg = "From: %s\nTo: %s\nSubject: %s\n\n%s"  % (fromaddr, toaddr, subj, msg_txt)

    srv.sendmail(fromaddr, toaddr, format(msg).encode('utf-8'))
    srv.quit()
    return

def send_telegram(text :str):
    token = "126xxxxx7695:AAFxxxxxxxxxxxtxxxxxxBz_xjguKLOFc"
    url = "https://api.telegram.org/bot"
    channel_id = 3510000007
    url += token
    method = url + "/sendMessage"

    r = requests.post(method, data={
         "chat_id": channel_id,
         "text": text,
          })

    if r.status_code != 200:
        raise Exception(r.text)

def test_date(dt_log):

    dt = datetime.datetime.today()
    ymd, hm = dt.strftime("%Y-%m-%d"), dt.strftime("%H:%M")
    #ymd, hm = '2020-09-22', '12:2' # DEBUG !

    if  dt_log[1:11] in ymd  and dt_log[12:16] in hm :
        return  True
    else:
         return  False #'2: now:'+ymd +' '+ hm +' dt_log:'+ dt_log[1:11]+dt_log[12:16]