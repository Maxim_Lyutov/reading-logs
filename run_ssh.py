import paramiko 

host = 'xxx-x.xxx.xx.local'
user = 'sxxxxx'
secret = 'xxxxxxpport'
port = 22

client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname=host, username=user, password=secret, port=port)

stdin, stdout, stderr = client.exec_command('cd ~/bin; ./monitor_logs.py')
data = stdout.read() + stderr.read()
print(data.decode("utf-8") )

client.close()